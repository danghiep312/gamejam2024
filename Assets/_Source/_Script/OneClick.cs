using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OneClick : MonoBehaviour
{
    public Button btn;
    

    private void OnEnable()
    {
        btn.interactable = true;
    }

    public void OnClick()
    {
        btn.interactable = false;
    }

}
