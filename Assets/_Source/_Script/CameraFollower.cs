using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Transform player;
    
    private void Start() {
        var vcam = GetComponent<CinemachineVirtualCamera>();
        vcam.LookAt = player;
        vcam.Follow = player;    
    }
}
