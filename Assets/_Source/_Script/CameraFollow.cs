using System;
using UnityEngine;

public class CameraFollow : Singleton<CameraFollow>
{
    private Vector3 offset = new Vector3(0f, 0f, -10f);
    private float smoothTime = 0.4f;
    private Vector3 velocity = Vector3.zero;

    [SerializeField] private Transform target;
    public static bool IsFollowing;
    
    public static void SetTarget(Transform target)
    {
        Instance.target = target;
    }
    
    private void Update()
    {
        if (!IsFollowing) return;
        Vector3 targetPosition = target.position + offset;
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }
    
}