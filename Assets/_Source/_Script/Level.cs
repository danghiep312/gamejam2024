using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Player player;

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (player == null) player = GetComponentInChildren<Player>(true);
    }
#endif
}
