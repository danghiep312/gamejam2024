﻿
using System;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    [SerializeField] private Player player;


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            player.IsGrounded = true;
            player.Rotation(false);
            player.OnCollisionWithGround();
        }

        if (other.gameObject.CompareTag("Trap"))
        {
            GameController.Instance.FinishLevel(false, player.GetStar());
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            player.IsGrounded = false;
        }
    }
    

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Finish"))
        {
            GameController.Instance.FinishLevel(player.score >= 70, player.GetStar());
        }

        if (other.gameObject.TryGetComponent(out Item item))
        {
            player.GetItem(item);
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        player = GetComponent<Player>();
        player.Rotation(false);
        player.OnCollisionWithGround();
    }
#endif
}