
using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerCollision _playerCollision;
    [SerializeField] private PlayerRotation _playerRotation;
    
    public float maxForce = 10f;
    public float forceSpeed = 1f;

    public float moveTimeMax = 2f;
    
    private bool _isHolding;
    private float _force = 0;
    public bool IsGrounded;

    private bool _isVulnerable = false;

    public float timeScaleJump = .7f;
    public float timeScaleMove = .4f;
    
    
    
    public GameObject jumpObject;
    [SerializeField] private Transform direction;
    [SerializeField] private Rigidbody2D rb;
    
    public int score = 50;

    public float timePenalty = 2f;
    public float _time;
    
    private Vector2 GetDirection() {
        return new Vector2(direction.position.x - transform.position.x,
            direction.position.y - transform.position.y);
    }
    
    private void AddScore(int value)
    {
        if (score > 0)
            score += value;
        this.PostEvent(EventID.ChangeHp, (float)score / 100);
    }

    public void GetItem(Item item)
    {
        AddScore(item.itemData.value);
        if (score <= 0)
        {
            this.PostEvent(EventID.Lose);
        }
    }

    private void Move()
    {
        CameraFollow.IsFollowing = true;
        Debug.Log("Move");
//        Debug.Log(direction.position.x + " " + transform.position.x + " " + direction.position.y + " " + transform.position.y);
        var dir = GetDirection();
        // Debug.Log(dir);
        // Debug.Break();
        GameController.SetTimeScale(1f);
        SetGravityScale(0f);
        Rotation(false);
        rb.velocity = Vector2.zero;
        rb.AddForce(dir.normalized * _force, ForceMode2D.Impulse);

        Util.Delay((_force / maxForce) * moveTimeMax, () =>
        {
            if (!_isVulnerable && !_isHolding && !IsGrounded)
            {
                rb.velocity = Vector2.right * (.1f * rb.velocity.x) + Vector2.up * rb.velocity.y;
                Rotation(true);
                SetGravityScale(1f);
            }
        });
    }

    private void OnEnable()
    {
        AddScore(0);
    }

    public void SetGravityScale(float gravity)
    {
        rb.gravityScale = gravity;
    }

    public void Jump()
    {
        Debug.Log("Jump");
        rb.AddForce(Vector2.up * _force, ForceMode2D.Impulse);
        Rotation(true);
        GameController.SetTimeScale(timeScaleJump);
        
        // rb.gravityScale = timeScaleJump;
    }

    public void Stop()
    {
        rb.simulated = false;
        GameController.SetTimeScale(1f);
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0f;
        Rotation(false);
    }
    
    private void Update()
    {
        jumpObject.transform.position = Vector3.up * (_force / maxForce + 1) + transform.position;
        direction.localPosition = Vector3.right * (_force / maxForce + 1);
        if (_isHolding)
        {
            _force += forceSpeed * Time.unscaledDeltaTime;
            _force = Mathf.Clamp(_force, 0, maxForce);
            if (!IsGrounded)
            {
                _playerRotation.enabled = true;
            }
        }
        else
        {
            jumpObject.SetActive(false);
        }
        
        if (Input.GetMouseButtonDown(0) && !Util.ClickOverUI())
        {
//            Debug.Log("Clivk");
            _isHolding = true;
            jumpObject.SetActive(IsGrounded);
            direction.gameObject.SetActive(!IsGrounded);

            if (!IsGrounded)
            {
                rb.velocity *= .1f;
                Rotation(true);
                GameController.SetTimeScale(timeScaleMove);
            }
        }
        else if (Input.GetMouseButtonUp(0) && !Util.ClickOverUI())
        {
            if (_isHolding)
            {
                if (IsGrounded)
                {
                    jumpObject.SetActive(false);
                    Jump();
                }
                else
                {
                    // rb.gravityScale = timeScaleMove;
                    direction.gameObject.SetActive(false);
                    Move();
                }
                _isHolding = false;
            }
            _force = 0;
        }

        _time += Time.deltaTime;
        if (_time >= timePenalty)
        {
            AddScore(-1);
            _time -= timePenalty;
        }
    }
    

    public void Rotation(bool status)
    {
        _playerRotation.enabled = status;
        direction.gameObject.SetActive(status);
        if (status)
        {
            GameController.SetTimeScale(timeScaleJump);
            // rb.velocity *= .1f;
        }
        else
        {
            // transform.rotation = quaternion.Euler(Vector3.zero);
            rb.angularVelocity = 0f;
            rb.velocity = Vector2.zero;
        }
    }
    
    public void OnCollisionWithGround()
    {
        _isHolding = false;
        transform.rotation = Quaternion.Euler(Vector3.zero);
        SetGravityScale(1f);
    }
    

#if UNITY_EDITOR
    private void OnValidate()
    {
        rb = GetComponentInChildren<Rigidbody2D>();
        _playerRotation = GetComponentInChildren<PlayerRotation>();
        _playerCollision = GetComponentInChildren<PlayerCollision>();
    }
#endif
    public int GetStar()
    {
        if (score >= 100)
            return 3;
        else if (score >= 90)
            return 2;
        else if (score >= 70)
            return 1;
        else
            return 0;
    }
}
