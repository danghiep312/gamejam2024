﻿

    using System;
    using UnityEngine;

    public class PlayerRotation : MonoBehaviour
    {
        [SerializeField] private Player _player;

        public float speed;

        private void Update()
        {
            transform.Rotate(Vector3.forward * (speed * Time.deltaTime));
        }
        

#if UNITY_EDITOR
        private void OnValidate()
        {
            _player = GetComponent<Player>();
        }
#endif
    }
