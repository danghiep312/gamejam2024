﻿using System.Security.Cryptography;
using Base.Sound;
using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class GameController : Singleton<GameController>
{
    public float timeScale;
    public GameObject currentLevel;

    public override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 90;
    }

    public static void SetTimeScale(float value)
    {
        Instance.timeScale = value;
        Time.timeScale = value;
        Time.fixedDeltaTime = value * 0.02f;
    }


    private void Update()
    {
        timeScale = Time.timeScale;
    }

    public async void LoadLevel(int level)
    {
        Camera.main.transform.position = new Vector3(0, 0, -10);
        var levelObj = Addressables.InstantiateAsync("Level" + level);
        levelObj.Completed += handle =>
        {
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                currentLevel = handle.Result;
                CameraFollow.SetTarget(currentLevel.GetComponent<Level>().player.transform);
            }
        };

        // var han = Addressables.InstantiateAsync("Player");
        // han.Completed += handle =>
        // {
        //     if (handle.Status == AsyncOperationStatus.Succeeded)
        //     {
        //         player = handle.Result.GetComponent<Player>();
        //         player.SetActive(true);
        //         CameraFollow.SetTarget(player.transform);
        //     }
        // };
        
        
        await levelObj.Task;
        // await han.Task;

        // CameraFollow.IsFollowing = true;
    }

    public void FinishLevel(bool isWin, int star)
    {
        currentLevel.GetComponent<Level>().player.Stop();
// <<<<<<< HEAD
//         if (isWin)
//             PanelManager.Instance.OpenPanel<WinPanel>(
//                 panelName: nameof(WinPanel), canBack: true,
//                 onSetup: async panel =>
//                 {
//                     // Time.timeScale = 0;
//                     await panel.Setup();
//                 }
//             ).Forget();
//         else
//         {
//             PanelManager.Instance.OpenPanel<LosePanel>(
//                 panelName: nameof(LosePanel), canBack: true,
//                 onSetup: async panel =>
//                 {
//                     // Time.timeScale = 0;
//                     await panel.Setup();
//                 }
//             ).Forget();
//         }
// =======
        Debug.Log("End game ne".Color("lime"));
        if (isWin)
        {
            GamePrefs.PassLevel(star);
            
        }

        PanelManager.Instance.ClosePanel(
            nameof(PlayPanel), immediately: false
        ).Forget();

        PanelManager.Instance.OpenPanel<EndGamePanel>(
            panelName: nameof(EndGamePanel), canBack: false,
            onSetup: async panel =>
            {
                await panel.Setup(isWin);

                SoundManager.StopAllMusic();
                SoundManager.PlayMusic(SoundName.ENDGAME, true);
            }
        ).Forget();
    }

    public void GoHome()
    {
        Time.timeScale = 1;
        CameraFollow.IsFollowing = false;
        

        Addressables.ReleaseInstance(currentLevel);
    }
}