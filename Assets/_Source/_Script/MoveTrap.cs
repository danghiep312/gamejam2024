using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MoveTrap : MonoBehaviour
{
    public Transform ground;

    public void OnEnable()
    {
        ground.DOLocalMoveY(-2, 1.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
    }
}
