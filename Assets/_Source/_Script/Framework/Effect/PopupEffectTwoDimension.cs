﻿using System;
using System.ComponentModel;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PopupEffectTwoDimension : MonoBehaviour
{
    public CanvasGroup panelToFade;
    
    [Title("Appear Effect")] 
    public AnimationCurve fadeCurve;
    [Space(20)]
    [HideLabel] [EnumToggleButtons]
    public DimensionType dimensionType;

    [ShowIf("dimensionType", DimensionType.Together, false)] [HorizontalGroup("TogetherDuration")]
    public float duration;
    [ShowIf("dimensionType", DimensionType.Together, false)] [HorizontalGroup("TogetherDuration")]
    public float delay;
    
    
    [ShowIf("dimensionType", DimensionType.Separate, false)] [HorizontalGroup("SeparateDurationX")]
    public float durationX, delayX;
    [ShowIf("dimensionType", DimensionType.Separate, false)] [HorizontalGroup("SeparateDurationY")]
    public float durationY, delayY;
    
    [Space(20)]
    [HideLabel, EnumToggleButtons]
    public AnimationType animationType;
    
    [ShowIf("@dimensionType == DimensionType.Together && animationType == AnimationType.Ease", false)] 
    public Ease animEase;
    [ShowIf("@dimensionType == DimensionType.Separate && animationType == AnimationType.Ease", false)]
    public Ease animEaseX, animEaseY;

    [ShowIf("@dimensionType == DimensionType.Together && animationType == AnimationType.AnimationCurve", false)] 
    public AnimationCurve animCurve;
    [ShowIf("@dimensionType == DimensionType.Separate && animationType == AnimationType.AnimationCurve", false)] 
    public AnimationCurve animCurveX, animCurveY;
    
    public Vector3 scaleValue;

    
    [Title("Disappear Effect")] 
    
    public AnimationCurve disFadeCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [Space(20)]
    [HideLabel] [EnumToggleButtons]
    public DimensionType disDimensionType;

    [ShowIf("disDimensionType", DimensionType.Together, false)][HorizontalGroup("DisTogetherDuration")]
    public float disDuration;
    [ShowIf("disDimensionType", DimensionType.Together, false)][HorizontalGroup("DisTogetherDuration")]
    public float disDelay;

    [ShowIf("disDimensionType", DimensionType.Separate, false)][HorizontalGroup("DisSeparateDurationX")]
    public float disDurationX, disDelayX;
    [ShowIf("disDimensionType", DimensionType.Separate, false)][HorizontalGroup("DisSeparateDurationY")]
    public float disDurationY, disDelayY;

    [Space(20)]
    [HideLabel, EnumToggleButtons]
    public AnimationType disAnimationType;
    
    
    [ShowIf("@disDimensionType == DimensionType.Together && disAnimationType == AnimationType.Ease", false)] 
    public Ease disAnimEase;
    [ShowIf("@disDimensionType == DimensionType.Separate && disAnimationType == AnimationType.Ease", false)]
    public Ease disAnimEaseX, disAnimEaseY;

    [ShowIf("@disDimensionType == DimensionType.Together && disAnimationType == AnimationType.AnimationCurve", false)] 
    public AnimationCurve disAnimCurve;
    [ShowIf("@disDimensionType == DimensionType.Separate && disAnimationType == AnimationType.AnimationCurve", false)] 
    public AnimationCurve disAnimCurveX, disAnimCurveY;
    
    public Vector3 disScaleValue;
    public GameObject targetDisable;

    
    [Space(50)]
    
    [Title("Enable callback")] 
    public UnityEvent enableCallback;
    public UnityEvent disableCallback;

    [Title("Complete Callback")] 
    public UnityEvent appearCallback;
    public UnityEvent disappearCallback;


    
    
    public void OnEnable()
    {
        //panelToFade.alpha = 0;
        enableCallback?.Invoke();
        Appear();
    }

    public void Appear()
    {
        DOTween.Kill(transform);
        transform.localScale = Vector3.zero;
        if (dimensionType == DimensionType.Together)
        {
            if (animationType == AnimationType.Ease) transform.DOScale(scaleValue, duration).SetEase(animEase).SetDelay(delay);
            else transform.DOScale(scaleValue, duration).SetEase(animCurve).SetDelay(delay);
        }
        else
        {
            if (animationType == AnimationType.Ease)
            {
                DOScaleAxis(0, scaleValue.x, durationX, delayX).SetEase(animEaseX);
                DOScaleAxis(1, scaleValue.y, durationY, delayY).SetEase(animEaseY).SetDelay(delayY);
            }
            else
            {
                DOScaleAxis(0, scaleValue.x, durationX, delayX).SetEase(animCurveX);
                DOScaleAxis(1, scaleValue.y, durationY, delayY).SetEase(animCurveY);
            }
        }

        panelToFade.alpha = 0;
        panelToFade.DOFade(1, DurationFade(dimensionType)).SetEase(fadeCurve).OnComplete(appearCallback.Invoke);
    }

    private float DurationFade(DimensionType type)
    {
        if (type == DimensionType.Together) return duration + delay;
        return durationX + delayX >= durationY + delayY ? durationX + delayX : durationY + delayY;
    }
    
    private TweenerCore<Vector3, Vector3, VectorOptions> DOScaleAxis(int axis, float value, float dur, float de)
    {
        if (axis == 0) return transform.DOScaleX(value, dur).SetDelay(de);
        return transform.DOScaleY(value, dur).SetDelay(de);
    }

    [Button]
    public void Disappearance()
    {
        disableCallback?.Invoke();
        try
        {
            if (dimensionType == DimensionType.Together)
            {
                if (disAnimationType == AnimationType.Ease)
                    transform.DOScale(disScaleValue, disDuration).SetEase(disAnimEase).SetDelay(disDelay);
                else transform.DOScale(disScaleValue, disDuration).SetEase(disAnimCurve).SetDelay(disDelay);
            }
            else
            {
                if (disAnimationType == AnimationType.Ease)
                {
                    DOScaleAxis(0, disScaleValue.x, disDurationX, disDelayX).SetEase(disAnimEaseX);
                    DOScaleAxis(1, disScaleValue.y, disDurationY, disDelayY).SetEase(disAnimEaseY);
                }
                else
                {
                    DOScaleAxis(0, disScaleValue.x, disDurationX, disDelayX).SetEase(disAnimCurveX);
                    DOScaleAxis(1, disScaleValue.y, disDurationY, disDelayY).SetEase(disAnimCurveY);
                }
            }

            panelToFade.DOFade(0, DurationFade(disDimensionType)).SetEase(disFadeCurve).OnComplete(() =>
            {
                disappearCallback?.Invoke();
                targetDisable.SetActive(false);
            });
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
            disappearCallback?.Invoke();
            targetDisable.SetActive(false);
        }
    }

    private void OnDisable()
    {
        DOTween.Kill(panelToFade);
    }


    private void OnValidate()
    {
        try
        {
            disAnimCurve ??= AnimationCurve.Linear(0, 0, 1, 1);
            fadeCurve ??= AnimationCurve.Linear(0, 0, 1, 1);
            // targetDisable = GetComponentInParent<Panel>().gameObject;
        }
        catch
        {
            // ignored
        }
    }

}

public enum DimensionType
{
    Together,
    Separate
}

public enum AnimationType
{
    Ease,
    AnimationCurve
}