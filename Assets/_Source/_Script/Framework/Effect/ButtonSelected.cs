using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonSelected : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    private Button _button;

    private void Start()
    {
        _button = GetComponent<Button>();
    }
    
    private void PointerEnter()
    {
        transform.localScale = Vector3.one * 0.95f;
    }
    
    private void PointerExit()
    {
        transform.localScale = Vector3.one;
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        PointerExit();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        PointerEnter();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //_button.onClick.Invoke();
    }
}
