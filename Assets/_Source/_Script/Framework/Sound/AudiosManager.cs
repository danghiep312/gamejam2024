using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public enum AudioType
{
    Sound,
    Music
}

[Serializable]
public class Audio
{
    public string name;
    public AudioType audioType;
    public AudioClip clip;
    [Range(0f, 1f)] public float volume = 1;
    [Range(.1f, 3f)] public float pitch = 1;
    /// <summary>
    /// If true, the audio is music, if false, the audio is sound
    /// </summary>
    public bool loop;
}

#if UNITY_EDITOR
[RequireComponent(typeof(DictionarySerialized))]
#endif

public class AudiosManager : Singleton<AudiosManager>
{
    #if UNITY_EDITOR
    public List<AudioClip> clips;
    #endif
    public List<Audio> audios;
    public List<AudioSource> audioStreams;
    public bool soundOn = true;
    public bool musicOn = true;
    
    public override void Awake()
    {
        base.Awake();
        audioStreams = new List<AudioSource>();
        soundOn = PlayerPrefs.GetInt("sound", 1) == 1;
        musicOn = PlayerPrefs.GetInt("music", 1) == 1;
    }

    public void PlaySoundEffect(String soundName)
    {
        if (!soundOn) return;
        soundName = Util.RemoveSpace(soundName);
        Audio audio = audios.Find(sound => sound.name.Equals(soundName));
        if (audio == null)
        {
            Common.LogWarning(this, "Can't find audio with name: " + soundName);
            return;
        }
        GetAndSetAudioSource(audio).PlayOneShot(audio.clip);
    }

    /// <summary>
    /// Check playing to check audio is playing or not
    /// </summary>
    /// <param name="checkPlaying"></param>
    /// <returns></returns>
    [Button("Play Sound")]
    public (AudioSource, float) Play(string soundName, bool loop = false, bool checkPlaying = false)
    {
        soundName = Util.RemoveSpace(soundName);
        Audio audio = audios.Find(sound => sound.name.Equals(soundName));
        if (audio == null)
        {
            Common.LogWarning(this, "Can't find audio with name: " + soundName);
            return (null, 0f);
        }
        
        if (audio.audioType == AudioType.Music && !musicOn) return (null, 0f);
        if (audio.audioType == AudioType.Sound && !soundOn) return (null, 0f);
        
        if (checkPlaying)
        {
            if (audioStreams.Any(stream => stream.clip.name.Equals(audio.clip.name)))
            {
                return (null, 0f);
            }
        }
        var source = GetAndSetAudioSource(audio);
        source.Play();
        source.loop = loop;
        return (source, source.clip.length);
    }

    public (AudioSource, float) Setup(string soundName)
    {
        soundName = Util.RemoveSpace(soundName);
        Audio audio = audios.Find(sound => sound.name.Equals(soundName));
        if (audio == null)
        {
            Common.LogWarning(this, "Can't find audio with name: " + soundName);
            return (null, 0f);
        }
        
        var source = GetAndSetAudioSource(audio);
        return (source, source.clip.length);
    }

    private AudioSource GetAndSetAudioSource(Audio audio)
    {
        AudioSource source = null;
        foreach (var stream in audioStreams.Where(stream => !stream.isPlaying))
        {
            source = stream;
        }

        if (!source)
        {
            source = gameObject.AddComponent<AudioSource>();
            audioStreams.Add(source);
        }

        source.clip = audio.clip;
        source.loop = audio.loop;
        source.volume = audio.volume;
        source.pitch = audio.pitch;
        
        return source;
    }

    public void Stop(string soundName)
    {
        Audio audio = audios.Find(sound => sound.name.Equals(soundName));
        if (audio == null)
        {
            Common.LogWarning(this, "Can't find audio with name: " + soundName);
            return;
        }

        foreach (var stream in 
                 audioStreams.Where(stream => stream.clip.name.Equals(audio.clip.name)))
        {
            stream.Stop();
        }
    }

    public void ChangePitch(float pitch)
    {
        foreach (var stream in audioStreams)
        {
            stream.pitch = pitch;
        }
    }

    public void StopAll(AudioType type = AudioType.Music)
    {
        foreach (var stream in audioStreams)
        {
            if (stream.loop)
                stream.Stop();
        }
    }
    
    public AudioClip GetClip(string clipName)
    {
        // return clip in audios
        clipName = Util.RemoveSpace(clipName);
        foreach (Audio audio in audios)
        {
            if (string.Equals(clipName, audio.name, StringComparison.OrdinalIgnoreCase))
            {
                return audio.clip;
            }
        }

        Debug.LogWarning("Can't find clip with name: " + clipName);
        return null;
    }

    private void SaveStatus()
    {
        PlayerPrefs.SetInt(AudioType.Sound.ToString().ToLower(), soundOn ? 1 : 0);
        PlayerPrefs.SetInt(AudioType.Music.ToString().ToLower(), musicOn ? 1 : 0);
    }
    
    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus) SaveStatus();
    }

    private void OnApplicationQuit()
    {
        SaveStatus();
    }

    #region tool

    
#if UNITY_EDITOR
    
    // [Button]
    // public void MappingAudio()
    // {
    //     if (TryGetComponent(out DictionarySerialized dict))
    //     {
    //         dict.audioMapping = new Dictionary<string, AudioClip>();
    //         dict.GetMapping(clips);
    //     }
    // }
    
    [Button]
    public void GetMappingAudio()
    {
        if (TryGetComponent(out DictionarySerialized dict))
        {
            audios.Clear();
            foreach (var key in dict.audioMapping.Keys)
            {
                audios.Add(new Audio()
                {
                    name = key,
                    clip = dict.audioMapping[key]
                });
            }
        }
    }

    [Button]
    public void SortAudioByName()
    {
        audios.Sort((audio1, audio2) => String.Compare(audio1.name, audio2.name, StringComparison.Ordinal));
    }

#endif

    #endregion
}


