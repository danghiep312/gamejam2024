using System;
using UnityEngine;
using UnityEngine.UI;


public class ClickSound : MonoBehaviour
{
    private Button _button;
    public string soundName;
    
    private void Start()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(Play);
    }

    public void Play()
    {
        AudiosManager.Instance.Play(soundName);
    }
}
