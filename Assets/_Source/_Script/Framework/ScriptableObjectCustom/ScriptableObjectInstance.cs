﻿


using UnityEngine;

public abstract class ScriptableObjectInstance : ScriptableObject
{
    public abstract void Init();
}

