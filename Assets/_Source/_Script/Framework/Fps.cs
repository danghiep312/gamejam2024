using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fps : MonoBehaviour
{
    private Text fps;

    private void Start()
    {
        fps = GetComponent<Text>();
    }

    private void Update()
    {
        if (Time.frameCount % (Application.targetFrameRate / 2) == 0)
        {
            fps.text = (int) (1 / Time.deltaTime) + "";
        }
    }
}
