using Base.Sound;
using Base.UI.Panel;
using UnityEngine;
using MoreMountains.NiceVibrations;

namespace Settings
{
    public enum ToggleType
    {
        Music,
        Sound,
        Vibration,
    }

    public class ToggleButton : MonoBehaviour
    {
        public ToggleType type;
        public UIToggle uiToggle;

        private bool IsOn()
        {
            return type switch
            {
                ToggleType.Music => SoundPrefs.MusicOn,
                ToggleType.Sound => SoundPrefs.SoundOn,
                ToggleType.Vibration => SoundPrefs.VibrationOn,
            };
        }

        private void Reset()
        {
            uiToggle = GetComponentInChildren<UIToggle>();
        }

        private void OnEnable()
        {
            // Sound.Controller.OnSoundChanged += OnSoundChanged;

            if (IsOn())
                uiToggle.TurnOn(immediate: true);
            else
                uiToggle.TurnOff(immediate: false);
        }

        public void OnClick()
        {
            if (IsOn())
                TurnOff();
            else
                TurnOn();
        }

        private void TurnOn()
        {
            uiToggle.TurnOn();

            switch (type)
            {
                case ToggleType.Music:
                    SoundPrefs.MusicOn = true;
                    SoundManager.PlaySound(SoundName.BUTTON_ON);
                    SoundManager.PlayMusic(SoundName.MENU, true);
                    break;
                case ToggleType.Sound:
                    SoundPrefs.SoundOn = true;
                    SoundManager.PlaySound(SoundName.BUTTON_ON);
                    break;
                case ToggleType.Vibration:
                    SoundPrefs.VibrationOn = true;
                    SoundManager.Vibrate(HapticTypes.Warning);
                    SoundManager.PlaySound(SoundName.BUTTON_ON);
                    break;
            }
        }

        private void TurnOff()
        {
            uiToggle.TurnOff();

            switch (type)
            {
                case ToggleType.Music:
                    SoundManager.PlaySound(SoundName.BUTTON_OFF);
                    SoundManager.StopAllMusic();
                    SoundPrefs.MusicOn = false;
                    break;
                case ToggleType.Sound:
                    SoundManager.PlaySound(SoundName.BUTTON_OFF);
                    SoundPrefs.SoundOn = false;
                    break;
                case ToggleType.Vibration:
                    SoundManager.PlaySound(SoundName.BUTTON_OFF);
                    SoundPrefs.VibrationOn = false;
                    break;
            }
        }
    }
}