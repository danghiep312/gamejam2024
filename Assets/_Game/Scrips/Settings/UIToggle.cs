using TMPro;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace Settings
{
    public class UIToggle : MonoBehaviour
    {
        [Title("Components")]
        public Image switchImage;
        public RectTransform switchRectTransform;
        public TextMeshProUGUI onText;
        public TextMeshProUGUI offText;

        [Title("Assets")]
        public Color activeColor;
        public Color inactiveColor;

        [Title("Animation")]
        public float targetAnchorPosX = 65f;
        public float duration = 0.25f;
        public Ease ease = Ease.OutQuart;

        private void Reset()
        {
            switchImage = transform.GetChild(0).GetComponent<Image>();
            switchRectTransform = switchImage.rectTransform;

            onText = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            offText = transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        }

        [Button]
        public void TurnOn(bool immediate = false)
        {
            switchImage.DOKill();
            switchRectTransform.DOKill();
            onText.DOKill();
            offText.DOKill();

            if (immediate)
            {
                switchImage.color = activeColor;
                switchRectTransform.anchoredPosition = new Vector2(targetAnchorPosX, 0f);
                onText.Fade(1f);
                offText.Fade(0f);
            }
            else
            {
                switchImage.DOColor(activeColor, duration).SetEase(ease);
                switchRectTransform.DOAnchorPosX(targetAnchorPosX, duration).SetEase(ease);
                onText.DOFade(1f, duration).SetEase(ease);
                offText.DOFade(0f, duration).SetEase(ease);
            }
        }

        [Button]
        public void TurnOff(bool immediate = false)
        {
            switchImage.DOKill();
            switchRectTransform.DOKill();
            onText.DOKill();
            offText.DOKill();

            if (immediate)
            {
                switchImage.color = inactiveColor;
                switchRectTransform.anchoredPosition = new Vector2(-targetAnchorPosX, 0f);
                onText.Fade(0f);
                offText.Fade(1f);
            }
            else
            {
                switchImage.DOColor(inactiveColor, duration).SetEase(ease);
                switchRectTransform.DOAnchorPosX(-targetAnchorPosX, duration).SetEase(ease);
                onText.DOFade(0f, duration).SetEase(ease);
                offText.DOFade(1f, duration).SetEase(ease);
            }
        }
    }
}