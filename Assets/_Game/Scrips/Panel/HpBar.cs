﻿
using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    public Image fill;

    public void SetFill(float value)
    {
        DOTween.Kill(fill);
        fill.DOFillAmount(value, .3f);
    }
}