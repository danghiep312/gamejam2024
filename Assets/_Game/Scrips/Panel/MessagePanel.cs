using System.Collections;
using System.Collections.Generic;
using Base.UI.Panel;
using TMPro;
using UnityEngine;

public class MessagePanel : PopupPanel
{
    public TextMeshProUGUI content;
    
    public void SetMessage(string message)
    {
        content.text = message;
    }
}
