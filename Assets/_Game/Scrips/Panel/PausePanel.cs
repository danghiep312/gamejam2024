using Base.Sound;
using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class PausePanel : PopupPanel
{
    public override UniTask Close()
    {
        // ... CUSTOM CODE HERE ...
        Time.timeScale = 1;

        return base.Close();
    }

    public void OnHomeButton()
    {
        DarkTransition.OnDarkTransitionAsync?.Invoke(async () =>
        {
            // reset game...
            GameController.Instance.GoHome();
            SoundManager.StopAllMusic();
            SoundManager.PlayMusic(SoundName.MENU);

            PanelManager.Instance.ClosePanel(nameof(PausePanel), immediately: false).Forget();

            await PanelManager.Instance.ClosePanel(nameof(PlayPanel), immediately: true);

            PanelManager.Instance.OpenPanel<HomePanel>(
                panelName: nameof(HomePanel), canBack: false,
                onSetup: async panel => await panel.Setup()
            ).Forget();
        });
    }
}