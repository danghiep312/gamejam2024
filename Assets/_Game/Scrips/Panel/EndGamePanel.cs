using Base.Sound;
using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePanel : Panel
{
    [Title("End Game")]
    public Image title;
    public Image duck;
    public Image nextButton;

    public Sprite winRibbon, duckWin, nextButtonSprite;
    public Sprite loseRibbon, duckLose, skipButtonSprite;

    [Title("Result")]
    public CanvasGroup resultCanvas;
    [ReadOnly] public RectTransform resultRect;
    [ReadOnly] public float resultTargetPos;

    [Title("Button Group")]
    public RectTransform buttonGroup;
    [ReadOnly] public float buttonTargetPos = -280;
    public float startPos = -700;

    protected override void OnValidate()
    {
        base.OnValidate();
        resultRect = resultCanvas.GetComponent<RectTransform>();
        resultTargetPos = resultRect.anchoredPosition.y;

        buttonTargetPos = buttonGroup.anchoredPosition.y;
    }

    public override async UniTask Setup(params object[] args)
    {
        var isWin = (bool)args[0];

        title.sprite = isWin ? winRibbon : loseRibbon;
        duck.sprite = isWin ? duckWin : duckLose;
        nextButton.sprite = isWin ? nextButtonSprite : skipButtonSprite;

        // animation
        await base.Setup(args);

        resultCanvas.DOKill();
        resultCanvas.Fade(0f);

        resultRect.DOKill();
        resultRect.DOAnchorPosY(resultTargetPos - 300f, 0f);

        buttonGroup.DOKill();
        buttonGroup.DOAnchorPosY(startPos, 0f);
    }

    public override async UniTask Open()
    {
        await base.Open();

        resultCanvas.DOFade(1f, openAnimationDuration);
        resultRect.DOAnchorPosY(resultTargetPos, openAnimationDuration)
            .SetEase(Ease.OutBack);

        buttonGroup.DOAnchorPosY(buttonTargetPos, 0.4f)
            .SetEase(Ease.OutQuad)
            .SetDelay(0.1f);
    }

    public void OnReplayButton()
    {
        Debug.Log("On Home Button".Color("red"));
        DarkTransition.OnDarkTransitionAsync?.Invoke(async () =>
        {
            GameController.Instance.GoHome();
            SoundManager.StopAllMusic();

            await PanelManager.Instance.ClosePanel(
                nameof(EndGamePanel), immediately: true
            );

            await PanelManager.Instance.OpenPanel<HomePanel>(
                nameof(HomePanel), canBack: false,
                onSetup: async panel =>
                {
                    await panel.Setup();
                    SoundManager.PlayMusic(SoundName.MENU, loop: true);
                }
            );
        });
    }

    public void OnNextButton()
    {
        Debug.Log("OnNextButton".Color("red"));
        DarkTransition.OnDarkTransitionAsync?.Invoke(async () =>
        {
            GameController.Instance.GoHome();
            SoundManager.StopAllMusic();

            await PanelManager.Instance.ClosePanel(
                nameof(EndGamePanel), immediately: true
            );

            await PanelManager.Instance.OpenPanel<PlayPanel>(
                nameof(PlayPanel), canBack: false,
                onSetup: async panel =>
                {
                    var level = GamePrefs.CurrentLevel();
                    GameController.Instance.LoadLevel(level);
                    CameraFollow.IsFollowing = true;
                    await panel.Setup(level);

                    SoundManager.PlayMusic(SoundName.GAMEPLAY, loop: true);
                }
            );
        });
    }
}