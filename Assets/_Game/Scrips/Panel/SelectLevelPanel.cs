﻿using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class SelectLevelPanel : Panel
{
    [Title("Select Level")]
    public LevelCell[] levelCells;

    protected override void Reset()
    {
        base.Reset();
        levelCells = GetComponentsInChildren<LevelCell>();
    }

    public override UniTask Setup(params object[] args)
    {
        base.Setup(args);

        for (var i = 0; i < levelCells.Length; i++)
        {
            var levelCell = levelCells[i];
            levelCell.Setup(i + 1);
            levelCell.DOKill();
            levelCell.transform.localScale = Vector3.zero;
        }

        return UniTask.CompletedTask;
    }

    public override async UniTask Open()
    {
        base.Open();

        foreach (var levelCell in levelCells)
        {
            levelCell.transform.DOScale(1f, 0.3f).SetEase(Ease.OutQuad);
            await UniTask.DelayFrame(2);
        }
    }

    public override async UniTask Close()
    {
        await base.Close();

        PanelManager.Instance.OpenPanel<HomePanel>(
            panelName: nameof(HomePanel), canBack: false,
            onSetup: async panel => await panel.Setup()
        ).Forget();
    }
}