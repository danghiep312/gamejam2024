using Base.Sound;
using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelCell : MonoBehaviour
{
    public TextMeshProUGUI levelName;
    public Image[] stars;

    [Header("Assets")]
    public Sprite starOn;
    public Sprite starOff;

    private int _level;

    public void Setup(int level)
    {
        _level = level;
        levelName.text = level.ToString();

        // check star of level
        var starCount = GamePrefs.StarCountOfLevel(level);
        foreach (var star in stars)
        {
            star.sprite = starCount > 0 ? starOn : starOff;
            starCount--;
        }
    }

    public void OnClick()
    {
        Debug.Log("Play Level : " + _level.ToString().Color("red"));
        if (_level > 3)
        {
            PanelManager.Instance.OpenPanel<MessagePanel>(
                panelName: nameof(MessagePanel), canBack: false,
                onSetup: async panel => await panel.Setup("Coming soon!")
            ).Forget();
            return;
        }
        
        DarkTransition.OnDarkTransitionAsync?.Invoke(async () =>
        {
            await PanelManager.Instance.ClosePanel(
                panelName: nameof(SelectLevelPanel), immediately: true
            );

            // load level
            SoundManager.StopAllMusic();
            SoundManager.PlayMusic(SoundName.GAMEPLAY, true);
            GameController.Instance.LoadLevel(_level);

            await PanelManager.Instance.OpenPanel<PlayPanel>(
                panelName: nameof(PlayPanel), canBack: false,
                onSetup: async panel => await panel.Setup(_level)
            );
        });
    }
}