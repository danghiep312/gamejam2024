using System;
using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class PlayPanel : Panel
{
    public TextMeshProUGUI levelName;
    public HpBar hpBar;
    
    private void OnEnable()
    {
        this.RegisterListener(EventID.ChangeHp, OnChangeHp);
    }
    
    private void OnDisable()
    {
        this.RemoveListener(EventID.ChangeHp, OnChangeHp);
    }

    private void OnChangeHp(object obj)
    {
        var hp = (float) obj;
        hp = math.min(hp, 1f);
        hpBar.SetFill(hp);
    }

    public override async UniTask Setup(params object[] args)
    {
        var level = (int)args[0];

        levelName.text = "Level " + level;

        await base.Setup(args);
    }

    public void OnPauseButton()
    {
        PanelManager.Instance.OpenPanel<PausePanel>(
            panelName: nameof(PausePanel), canBack: true,
            onSetup: async panel => await panel.Setup()
        ).Forget();
    }
    
    
}