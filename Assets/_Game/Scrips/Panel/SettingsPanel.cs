using Base.UI.Panel;
using Cysharp.Threading.Tasks;

public class SettingsPanel : PopupPanel
{
    public override async UniTask Close()
    {
        await base.Close();

        PanelManager.Instance.OpenPanel<HomePanel>(
            panelName: nameof(HomePanel), canBack: false,
            onSetup: async panel => await panel.Setup()
        ).Forget();
    }
}