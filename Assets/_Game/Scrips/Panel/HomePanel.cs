﻿using Base.Sound;
using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class HomePanel : Panel
{
    [Title("Home Panel")]
    public RectTransform[] buttons;
    public float offset = 200f;

    public override UniTask Setup(params object[] args)
    {
        base.Setup(args);

        // setup button
        for (var i = 0; i < buttons.Length; i++)
        {
            var x = (i % 2 == 0 ? 1 : -1) * offset;
            var y = buttons[i].anchoredPosition.y;

            buttons[i].DOKill();
            buttons[i].anchoredPosition = new Vector2(x, y);
        }

        return UniTask.CompletedTask;
    }

    public override UniTask Open()
    {
        base.Open();

        foreach (var button in buttons)
        {
            button.DOAnchorPosX(0f, openAnimationDuration)
                .SetEase(Ease.OutBounce);
        }

        return UniTask.CompletedTask;
    }

    public void OnPlayGame()
    {
        DarkTransition.OnDarkTransitionAsync?.Invoke(async () =>
        {
            await PanelManager.Instance.ClosePanel(
                panelName: nameof(HomePanel), immediately: true
            );

            // load level
            SoundManager.StopAllMusic();
            SoundManager.PlayMusic(SoundName.GAMEPLAY, true);
            GameController.Instance.LoadLevel(GamePrefs.CurrentLevel());

            await PanelManager.Instance.OpenPanel<PlayPanel>(
                panelName: nameof(PlayPanel), canBack: false,
                onSetup: async panel => await panel.Setup(GamePrefs.CurrentLevel())
            );
        });
    }

    public void OnSelectLevel()
    {
        PanelManager.Instance.ClosePanel(
            panelName: nameof(HomePanel)
        ).Forget();

        PanelManager.Instance.OpenPanel<SelectLevelPanel>(
            panelName: nameof(SelectLevelPanel), canBack: false,
            onSetup: async panel => await panel.Setup()
        ).Forget();
    }

    public void OnSettings()
    {
        PanelManager.Instance.ClosePanel(
            panelName: nameof(HomePanel)
        ).Forget();

        PanelManager.Instance.OpenPanel<PopupPanel>(
            panelName: "SettingsPanel", canBack: false,
            onSetup: async panel => await panel.Setup()
        ).Forget();
    }
}