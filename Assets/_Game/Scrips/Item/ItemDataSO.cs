using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "ItemDataSO", menuName = "Game Data/Item Data")]
public class ItemDataSO : ScriptableObject
{
    public int itemID;
    public string itemName;
    public int value;

    [Header("SFX")]
    public string soundName;
    public string effectName;
    public float effectScale = 1f;

    private void OnValidate()
    {
#if UNITY_EDITOR
        var fileName = (value > 0 ? "Buff_" : "Neft_") + itemID + "_" + itemName.Replace(" ", "");
        var path = AssetDatabase.GetAssetPath(this);
        AssetDatabase.RenameAsset(path, fileName);
#endif
    }

    public override string ToString()
    {
        return $"ID: {itemID}\n" +
               $"Name: {itemName}\n" +
               $"Value: {value}\n";
    }
}