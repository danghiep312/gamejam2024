﻿using System;
using Base.Pool;
using Base.Sound;
using Sirenix.OdinInspector;
using UnityEngine;

public class Item : MonoBehaviour
{
    public ItemDataSO itemData;

    private void OnValidate()
    {
        if (itemData != null && gameObject.name != itemData.name)
            gameObject.name = itemData.name;
    }

    [Button]
    public void Destroy()
    {
        SoundManager.PlaySoundOneShot(itemData.soundName);

        var effect = PoolManager.Instance.Spawn(itemData.effectName, transform.position, Quaternion.identity)
            .GetComponent<ParticleSystem>();
        effect.transform.localScale = Vector3.one * itemData.effectScale;
        effect.Play();

        Destroy(gameObject);
    }
}