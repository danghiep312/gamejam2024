﻿using UnityEngine;

public class _PlayerCollect : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Item item))
        {
            Collect(item.itemData);
            item.Destroy();
        }
    }

    public void Collect(ItemDataSO itemData)
    {
        Debug.Log($"Collected {itemData.itemName}");
        // apply something...
    }
}