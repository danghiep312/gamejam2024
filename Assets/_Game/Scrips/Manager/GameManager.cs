using Base.Singleton;
using Base.Sound;
using Base.UI.Panel;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

public class GameManager : SingletonPersistent<GameManager>
{
    protected override void OnAwake()
    {
    }

    private void Start()
    {
        PanelManager.Instance.OpenPanel<HomePanel>(
            panelName: nameof(HomePanel), canBack: false,
            onSetup: async panel =>
            {
                await panel.Setup();
                SoundManager.PlayMusic(SoundName.MENU, loop: true);
            }
        ).Forget();
    }

    public bool isWin = true;

    [Button("End Game")]
    public void WinGame()
    {
        
    }
}