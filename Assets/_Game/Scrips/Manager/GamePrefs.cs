using UnityEngine;

public static class GamePrefs
{
    public static int CurrentLevel()
    {
        return PlayerPrefs.GetInt("CurrentLevel", 1);
    }

    public static void PassLevel(int starCount)
    {
        SetStarCountOfLevel(CurrentLevel(), starCount);
        PlayerPrefs.SetInt("CurrentLevel", CurrentLevel() + 1);
    }

    public static int StarCountOfLevel(int level)
    {
        return PlayerPrefs.GetInt($"Level_{level}_Star", 0);
    }

    public static void SetStarCountOfLevel(int level, int starCount)
    {
        PlayerPrefs.SetInt($"Level_{level}_Star", starCount);
    }
}