using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Base.UI.Animation
{
    public class AnimButtonPress : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
    {
        public float scale = 0.95f;
        public float duration = 0.25f;

        public void OnPointerDown(PointerEventData eventData)
        {
            transform.DOKill();
            transform.DOScale(scale, duration).SetEase(Ease.OutCubic);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            transform.DOKill();
            transform.DOScale(1f, duration).SetEase(Ease.InCubic);
        }
    }
}