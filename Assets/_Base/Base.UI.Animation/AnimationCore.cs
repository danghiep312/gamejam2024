using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;

namespace Base.UI.Animation
{
    public abstract class AnimationCore : MonoBehaviour
    {
        [Title("Delay on enable")]
        public bool autoPlayOnEnable = true;

        [ShowIf("@autoPlayOnEnable")]
        public bool delayOnEnable = true;

        [ShowIf("@autoPlayOnEnable && delayOnEnable")]
        public float delayDuration = 3f;

        protected Sequence animationSequence;

        protected async void OnEnable()
        {
            // not auto play animation
            if (!autoPlayOnEnable)
                return;

            // delay before play animation
            if (delayOnEnable)
                await UniTask.Delay((int)(delayDuration * 1000));

            // play animation (check if gameObject is still active)
            if (gameObject.activeInHierarchy)
                StartAnimation();
        }

        protected void OnDisable()
        {
            StopAnimation();
        }

        public abstract void StartAnimation();

        public virtual void StopAnimation()
        {
            animationSequence?.Kill();
        }
    }
}