using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

namespace Base.UI.Animation
{
    public class AnimMoveInCycle : AnimationCore
    {
        [Title("Move in cycle")]
        public RectTransform rectTransform;
        public float amplitude = 15f;
        public float duration = 0.9f;
        public float delayBetweenCycles = 0.1f;

        [Space]
        [ReadOnly] public float initialPosition;
        [ReadOnly] public float targetPosition;

        private void OnValidate()
        {
            rectTransform = GetComponent<RectTransform>();
            initialPosition = rectTransform.anchoredPosition.y;
            targetPosition = initialPosition + amplitude;
        }

        public override void StartAnimation()
        {
            animationSequence = DOTween.Sequence();

            animationSequence.Append(rectTransform.DOAnchorPosY(targetPosition, duration).SetEase(Ease.InOutSine));

            animationSequence.AppendInterval(delayBetweenCycles);

            animationSequence.Append(rectTransform.DOAnchorPosY(initialPosition, duration).SetEase(Ease.InOutSine));

            animationSequence.Play().SetLoops(-1, LoopType.Yoyo);
        }
    }
}