﻿using DG.Tweening;
using Sirenix.OdinInspector;

namespace Base.UI.Animation
{
    public class AnimScaleLoop : AnimationCore
    {
        [Title("Scale loop")]
        public float targetScale = 1.1f;
        public float scaleDuration = 0.2f;
        public int numberOfScale = 2;
        public float delay = 1.8f;

        public override void StartAnimation()
        {
            animationSequence = DOTween.Sequence();
        
            animationSequence.Append(transform.DOScale(targetScale, scaleDuration)
                .SetEase(Ease.InOutQuad).SetLoops(numberOfScale, LoopType.Yoyo));

            animationSequence.AppendInterval(delay);

            animationSequence.Play().SetLoops(-1, LoopType.Yoyo);
        }
    }
}