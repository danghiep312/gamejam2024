using DG.Tweening;
using UnityEngine;

namespace Base.UI.Animation
{
    public class AnimRotateLoop : AnimationCore
    {
        public float rotateDuration = 10f;

        public override void StartAnimation()
        {
            animationSequence = DOTween.Sequence();

            animationSequence.Append(transform.DORotate(Vector3.forward * -360, rotateDuration, RotateMode.FastBeyond360)
                .SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart));

            animationSequence.Play().SetLoops(-1, LoopType.Yoyo);
        }
    }
}