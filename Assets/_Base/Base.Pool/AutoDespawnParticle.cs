using UnityEngine;

namespace Base.Pool
{
    /// <summary>
    /// Attach this script to a particle system to automatically despawn it when it stops playing.
    /// Must select "Stop Action" = "Callback".
    /// </summary>
    public class AutoDespawnParticle : MonoBehaviour
    {
        private void OnParticleSystemStopped()
        {
            PoolManager.Instance.Despawn(gameObject);
        }
    }
}