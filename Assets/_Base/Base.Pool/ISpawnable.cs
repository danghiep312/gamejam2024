namespace Base.Pool
{
    public interface ISpawnable
    {
        void OnSpawn();
        void OnDespawn();
    }
}