﻿using UnityEngine;
using System.Collections.Generic;

namespace Base.Pool
{
    [CreateAssetMenu(fileName = nameof(PoolDataSO), menuName = "GameData/PoolData")]
    public partial class PoolDataSO : ScriptableObject
    {
        public List<GameObject> prefabs;
    }
}