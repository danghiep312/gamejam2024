namespace Base.Pool
{
	public static class PoolName
	{
		public const string VFX_COIN = "VFX_Coin";
		public const string CFXR_EXPLOSION = "CFXR Explosion";
		public const string VFX_BUBBLE_BREAK = "VFX_BubbleBreak";
	}
}
