﻿using UnityEngine;
using Sirenix.Utilities;
using System.Collections.Generic;
using Base.Singleton;

namespace Base.Pool
{
    public class PoolManager : Base.Singleton.Singleton<PoolManager>
    {
        [SerializeField]
        private PoolDataSO poolData;

        private readonly Dictionary<string, List<GameObject>> _poolTable = new Dictionary<string, List<GameObject>>();

        protected override void OnAwake()
        {
            InitPoolTable();
        }

        private void InitPoolTable()
        {
            poolData.prefabs.ForEach(prefab => _poolTable.Add(prefab.name, new List<GameObject>()));
        }

        public GameObject Spawn(string prefabName)
        {
            // check if pool not contains prefabName
            if (!_poolTable.ContainsKey(prefabName))
            {
                Debug.LogError($"Instance with name + " + prefabName.Color("red") + " is not exist in pool!");
                return null;
            }

            // find object not active in hierarchy and is not child of this (PoolManager)
            var goInactive = _poolTable[prefabName].Find(go => !go.activeInHierarchy && go.transform.parent == this.transform);
            if (goInactive != null)
            {
                goInactive.SetActive(true);
                return goInactive;
            }

            // create new object, expand list object
            var newGo = Instantiate(GetPrefabByName(prefabName));
            _poolTable[prefabName].Add(newGo);
            newGo.SetActive(true);
            return newGo;
        }

        public GameObject Spawn(string prefabName, Transform parent)
        {
            var go = Spawn(prefabName);

            if (go != null)
            {
                go.transform.SetParent(parent);
            }

            return go;
        }

        public GameObject Spawn(string prefabName, Vector3 position, Quaternion rotation)
        {
            var go = Spawn(prefabName);

            if (go != null)
            {
                go.transform.position = position;
                go.transform.rotation = rotation;
            }

            return go;
        }

        public GameObject Spawn(string prefabName, Vector3 position, Quaternion rotation, Transform parent)
        {
            var go = Spawn(prefabName);

            if (go != null)
            {
                go.transform.SetParent(parent);
                go.transform.position = position;
                go.transform.rotation = rotation;
            }

            return go;
        }

        public void Despawn(GameObject go)
        {
            go.SetActive(false);
            go.transform.SetParent(transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = Vector3.zero;

            if (go.TryGetComponent(out ISpawnable despawnable))
                despawnable.OnDespawn();
        }

        public void DespawnAll()
        {
            Debug.Log("[PoolManager] recall all game objects!".Color("orange"));
            _poolTable.Values.ForEach(listGo => { listGo.ForEach(Despawn); });
        }

        public GameObject GetPrefabByName(string prefabName)
        {
            return poolData.prefabs.Find(prefab => prefab.name.Equals(prefabName));
        }
    }
}