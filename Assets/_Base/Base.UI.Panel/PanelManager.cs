using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Base.UI.Panel
{
    public class PanelManager : Singleton<PanelManager>
    {
        [ShowInInspector]
        private readonly List<Panel> _stackPanels = new List<Panel>();


        public T GetPanel<T>(string panelName) where T : Panel
        {
            return (T)_stackPanels.Find(panel => panel.PanelName == panelName);
        }


        public Type CurrentPanelType => _stackPanels.Count > 0
            ? _stackPanels.Last().GetType()
            : null;



        public async UniTask OpenPanel<T>(string panelName, bool canBack, Func<T, UniTask> onSetup) where T : Panel
        {
            Debug.Log($"[PanelManager] Create {panelName}");
            var panel = (await Addressables.InstantiateAsync(panelName, transform)).GetComponent<T>();
            panel.Init(panelName, canBack);
            _stackPanels.Add(panel);

            await onSetup(panel);
            await panel.Open();
        }


        public async UniTask ClosePanel(string panelName, bool immediately = false)
        {
            var panelOfType = _stackPanels.Find(panel => panel.PanelName == panelName);

            if (panelOfType == null)
            {
                Debug.LogError("[PanelManager] Cannot find panel type " + panelName.Color("red"));
                return;
            }

            Debug.Log($"[PanelManager] Close {panelName}");
            if (immediately)
                panelOfType.CloseImmediately();
            else
                await panelOfType.Close();
        }


        public void ReleasePanel(Panel panelClosed)
        {
            Debug.Log("[PanelManager] Release " + panelClosed.PanelName);
            _stackPanels.Remove(panelClosed);
        }


        private void TryCloseCurrentPanel()
        {
            if (_stackPanels.Count == 0)
            {
                Debug.LogWarning("[PanelManager] Stack is empty");
                return;
            }

            if (!_stackPanels.Last().CanBack)
            {
                Debug.LogWarning("[PanelManager] Cannot back");
                return;
            }

            Debug.Log("[PanelManager] Close " + CurrentPanelType.Name.Color("cyan"));
            var panelInTop = _stackPanels.Last();
            _stackPanels.Remove(panelInTop);
            panelInTop.Close();
        }
    }
}