using Cysharp.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Base.UI.Panel
{
    [RequireComponent(typeof(CanvasGroup))]
    public class Panel : MonoBehaviour
    {
        [ShowInInspector, ReadOnly, PropertyOrder(-1)]
        public string PanelName { get; private set; }

        [ShowInInspector, ReadOnly, PropertyOrder(-1)]
        public bool CanBack { get; private set; }


        [Title("Panel")]
        [InlineButton(nameof(ToggleVisibility))]
        public CanvasGroup panelCanvasGroup;
        public float openAnimationDuration = 0.4f;
        public float closeAnimationDuration = 0.3f;


        public void Init(string panelName, bool canBack)
        {
            PanelName = panelName;
            CanBack = canBack;
        }


        public virtual UniTask Setup(params object[] args)
        {
            gameObject.SetActive(true);
            panelCanvasGroup.alpha = 0f;
            panelCanvasGroup.interactable = false;

            return UniTask.CompletedTask;
        }


        public virtual UniTask Open()
        {
            panelCanvasGroup.DOFade(1f, openAnimationDuration)
                .OnComplete(() => panelCanvasGroup.interactable = true);

            return UniTask.CompletedTask;
        }


        public virtual UniTask Close()
        {
            panelCanvasGroup.interactable = false;
            panelCanvasGroup.DOFade(0f, closeAnimationDuration)
                .OnComplete(OnCloseCompleted);

            return UniTask.CompletedTask;
        }


        public virtual void CloseImmediately()
        {
            OnCloseCompleted();
        }


        protected virtual void OnCloseCompleted()
        {
            PanelManager.Instance.ReleasePanel(this);
            Destroy(gameObject);
        }


        public void OnOpenButton() => Open();

        public void OnCloseButton() => Close();


        #region ===== EDITOR =====
        protected virtual void ToggleVisibility()
        {
            panelCanvasGroup.alpha = (panelCanvasGroup.alpha == 0) ? 1 : 0;
        }

        protected virtual void OnValidate()
        {
        }

        protected virtual void Reset()
        {
            panelCanvasGroup = GetComponent<CanvasGroup>();
        }
        #endregion
    }
}