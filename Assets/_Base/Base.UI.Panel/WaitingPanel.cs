﻿using Cysharp.Threading.Tasks;
using TMPro;

namespace Base.UI.Panel
{
    public class WaitingPanel : Panel
    {
        public TextMeshProUGUI messageTMP;

        public override UniTask Setup(params object[] args)
        {
            var message = (string)args[0];

            base.Setup();
            messageTMP.text = message;
            return UniTask.CompletedTask;
        }
    }
}