using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class DarkTransition : MonoBehaviour
{
    public delegate void OnTransitionAsync(Func<UniTask> callback);
    public static OnTransitionAsync OnDarkTransitionAsync;

    public delegate void OnTransition(Action callback);
    public static OnTransition OnDarkTransition;

    public Image darkImage;
    public float fadeDuration = 0.3f;

    private void Reset()
    {
        darkImage = GetComponent<Image>();
    }

    private void Awake()
    {
        OnDarkTransitionAsync += TransitionAsync;
        OnDarkTransition += Transition;
    }

    private void OnDestroy()
    {
        OnDarkTransitionAsync -= TransitionAsync;
        OnDarkTransition -= Transition;
    }

    private void Start() => Hide();

    private void Hide()
    {
        gameObject.SetActive(false);
        darkImage.Fade(0f);
    }

    private async UniTask FadeIn()
    {
        gameObject.SetActive(true);
        await darkImage.DOFade(1f, fadeDuration)
            .ToUniTask();
    }

    private async UniTask FadeOut()
    {
        await darkImage.DOFade(0f, fadeDuration)
            .OnComplete(Hide)
            .ToUniTask();
    }

    private async void TransitionAsync(Func<UniTask> callBack)
    {
        await FadeIn();
        await callBack();
        await FadeOut();
    }

    private async void Transition(Action callBack)
    {
        await FadeIn();
        callBack?.Invoke();
        await FadeOut();
    }
}