﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace Base.UI.Panel
{
    [ExecuteAlways]
    public class PopupPanel : Panel
    {
        [Title("Background (named correctly)")]
        public Image background;
        public float targetAlpha = 0.5f;

        [Title("Popup (named correctly)")]
        public Transform popupTransform;
        public CanvasGroup popupCanvas;

        #region === Inspector ===
        protected override void OnValidate()
        {
            base.OnValidate();
            background.Fade(targetAlpha);
        }

        protected override void Reset()
        {
            base.Reset();
            background = transform.Find("Background").GetComponent<Image>();
            popupTransform = transform.Find("Popup");
            popupCanvas = popupTransform.GetComponent<CanvasGroup>();
        }
        #endregion

        public override UniTask Setup(params object[] args)
        {
            gameObject.SetActive(true);

            background.DOKill();
            background.Fade(0f);

            popupCanvas.DOKill();
            popupCanvas.alpha = 0;
            popupCanvas.interactable = false;

            popupTransform.DOKill();
            popupTransform.localScale = Vector3.one * 0.5f;

            return UniTask.CompletedTask;
        }

        public override UniTask Open()
        {
            background.DOKill();
            background.DOFade(targetAlpha, openAnimationDuration)
                .SetEase(Ease.OutCubic);

            popupTransform.DOKill();
            popupTransform.DOScale(1f, openAnimationDuration)
                .SetEase(Ease.OutBack);

            popupCanvas.DOKill();
            popupCanvas.DOFade(1f, openAnimationDuration)
                .SetEase(Ease.OutCubic)
                .OnComplete(() => popupCanvas.interactable = true);

            return UniTask.CompletedTask;
        }

        public override UniTask Close()
        {
            background.DOKill();
            background.DOFade(0f, closeAnimationDuration)
                .SetEase(Ease.OutCubic)
                .OnComplete(OnCloseCompleted);

            popupCanvas.interactable = false;
            popupCanvas.DOKill();
            popupCanvas.DOFade(0f, closeAnimationDuration)
                .SetEase(Ease.OutCubic);

            popupTransform.DOKill();
            popupTransform.DOScale(0.5f, closeAnimationDuration)
                .SetEase(Ease.InBack);

            return UniTask.CompletedTask;
        }
    }
}