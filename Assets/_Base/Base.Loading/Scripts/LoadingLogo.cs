using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LoadingLogo : MonoBehaviour
{
    private Image _image;
    private RectTransform _rectTransform;
    private float _targetAnchorPosY;

    private void Awake()
    {
        _image = GetComponent<Image>();
        _rectTransform = GetComponent<RectTransform>();
        _targetAnchorPosY = _rectTransform.anchoredPosition.y;
    }

    public void Reset()
    {
        _image.Fade(0f);
        _rectTransform.DOAnchorPosY(_targetAnchorPosY - 300f, 0.001f);
    }

    public void Show()
    {
        _image.DOKill();
        _image.DOFade(1f, 0.5f);

        _rectTransform.DOKill();
        _rectTransform.DOAnchorPosY(_targetAnchorPosY, 0.5f).SetEase(Ease.OutBack);
    }
}