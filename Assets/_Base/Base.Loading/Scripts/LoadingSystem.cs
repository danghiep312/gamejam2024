﻿using Base.Singleton;
using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;

public class LoadingSystem : Singleton<LoadingSystem>
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private LoadingLogo loadingLogo;
    [SerializeField] private LoadingBar loadingBar;

    [SerializeField] private bool isFirstTimeLoading = true;
    [ShowInInspector, ReadOnly] private float _firstLoadingDuration = 5f;
    [ShowInInspector, ReadOnly] private float _normalLoadingDuration = 1f;
    

    private void Start()
    {
        // reset all
        canvasGroup.gameObject.SetActive(true);
        canvasGroup.alpha = 1f;
        loadingLogo.Reset();
        loadingBar.Reset();

// #if UNITY_EDITOR
//         // If in EDITOR, need quick loading
//         _firstLoadingDuration = _normalLoadingDuration;
// #endif

        //! Start in scene Loading => Main
        LoadSceneAsync("Main");
    }

    public async void LoadSceneAsync(string sceneName)
    {
        // Start loading the scene
        var scene = SceneManager.LoadSceneAsync(sceneName);
        scene.allowSceneActivation = false;

        // Show the loading screen
        canvasGroup.gameObject.SetActive(true);
        canvasGroup.DOFade(1f, 0.3f);
        await UniTask.Delay(300);
        loadingLogo.Show();

        do
        {
            //! First time loading => need more time, else just normal
            var duration = isFirstTimeLoading ? _firstLoadingDuration : _normalLoadingDuration;
            loadingBar.UpdateProgress(scene.progress / 0.9f, duration);
            await UniTask.Delay(duration.Millisecond());
        } while (scene.progress < 0.9f);

        // Activate the next scene
        scene.allowSceneActivation = true;
        await UniTask.Delay(300);

        canvasGroup.DOFade(0f, 0.3f).OnComplete(() =>
        {
            isFirstTimeLoading = false;
            canvasGroup.gameObject.SetActive(false);
            loadingLogo.Reset();
            loadingBar.Reset();
        });
    }
}