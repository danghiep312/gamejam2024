using System;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Base.Tool.Sheet
{
    [Serializable]
    public class SpreadsheetUrl
    {
        [InfoBox("This spreadsheet will not be fetched", InfoMessageType.Warning, "@!enabledFetch")]
        public bool enabledFetch = true;

        [HideIf("@!enabledFetch")]
        [InlineButton(nameof(OpenOriginalSheet), "Open")]
        public string originalUrl;

        [HideIf("@!enabledFetch")]
        [InlineButton(nameof(OpenDeploySheet), "Open")]
        public string deployUrl;

        [HideIf("@!enabledFetch"), Space]
        public UnityEvent<SpreadsheetData> onFetchedData;

        public SpreadsheetUrl(string originalUrl, string deployUrl)
        {
            this.originalUrl = originalUrl;
            this.deployUrl = deployUrl;
        }

        private void OpenOriginalSheet() => Application.OpenURL(this.originalUrl);

        private void OpenDeploySheet() => Application.OpenURL(this.deployUrl);
    }
}