namespace Base.Tool.Sheet
{
    // TODO: implement this interface in script load data to game after fetch from sheet
    public interface IFetchFromSheet
    {
        void OnDataFetched(SpreadsheetData spreadsheet);
    }
}