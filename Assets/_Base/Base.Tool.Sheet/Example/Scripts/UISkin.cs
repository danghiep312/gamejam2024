using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Base.Tool.Sheet.Example
{
    public class UISkin : MonoBehaviour
    {
        public Image previewImage;
        public TextMeshProUGUI idText;
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI priceText;

        public void Setup(SkinData skinData)
        {
            this.previewImage.sprite = skinData.preview;
            this.idText.text = "ID: " + skinData.id;
            this.nameText.text = "Name: " + skinData.name;
            this.priceText.text = "Price: " + skinData.price;
        }
    }
}