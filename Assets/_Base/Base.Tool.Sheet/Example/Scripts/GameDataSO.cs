using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Base.Tool.Sheet.Example
{
    [CreateAssetMenu(fileName = "GameDataSO", menuName = "Sheet/GameDataSO")]
    public class GameDataSO : ScriptableObject, IFetchFromSheet
    {
        public List<SkinData> skinDataSet = new List<SkinData>();

        public void OnDataFetched(SpreadsheetData spreadsheet)
        {
            Debug.Log("Load data into game".Color("yellow"));

            // lấy dữ liệu từ sheet Skin (sheet 0)
            var sheetItem = spreadsheet.GetSheet(0);

            this.skinDataSet.Clear();
            for (var i = 1; i < sheetItem.Row; i++)
            {
                var newItem = new SkinData()
                {
                    // cell(i, 0) là ảnh trên sheet, không dùng trong code
                    id = int.Parse(sheetItem.GetCell(i, 1)),
                    name = sheetItem.GetCell(i, 2),
                    price = int.Parse(sheetItem.GetCell(i, 3)),
                    preview = Resources.Load<Sprite>(sheetItem.GetCell(i, 4)),
                };
                this.skinDataSet.Add(newItem);
            }

#if UNITY_EDITOR
            // Save the changes to the asset database
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
#endif
        }
    }
}