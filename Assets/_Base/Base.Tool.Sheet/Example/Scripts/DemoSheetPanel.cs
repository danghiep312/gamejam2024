using UnityEngine;

namespace Base.Tool.Sheet.Example
{
    public class DemoSheetPanel : MonoBehaviour
    {
        public GameDataSO gameData;
        public GameObject uiSkinPrefab;
        public Transform content;

        private void Start()
        {
            PrepareUISkins();
        }

        private void PrepareUISkins()
        {
            // clear item in scene
            foreach (Transform child in this.content)
                Destroy(child.gameObject);

            // instantiate new item
            var itemDataSet = this.gameData.skinDataSet;
            foreach (var itemData in itemDataSet)
            {
                var uiItem = Instantiate(this.uiSkinPrefab, this.content).GetComponent<UISkin>();
                uiItem.Setup(itemData);
            }
        }
    }
}