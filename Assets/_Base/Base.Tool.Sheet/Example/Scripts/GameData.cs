using System;
using UnityEngine;

namespace Base.Tool.Sheet.Example
{
    [Serializable]
    public class SkinData
    {
        public int id;
        public string name;
        public int price;
        public Sprite preview;
    }
}