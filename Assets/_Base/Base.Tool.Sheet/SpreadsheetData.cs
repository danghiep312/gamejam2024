﻿using System;

namespace Base.Tool.Sheet
{
    [Serializable]
    public class SpreadsheetData
    {
        public SheetData[] sheets = { };

        public int SheetCount => this.sheets.Length;

        public SheetData GetSheet(int index)
        {
            return this.sheets[index];
        }
    }

    [Serializable]
    public class SheetData
    {
        public string[,] table = { };

        public int Row => this.table.GetLength(0);

        public int Col => this.table.GetLength(1);

        public string GetCell(int row, int column)
        {
            return this.table[row, column];
        }
    }
}

/*
 * 1 spreadsheet gồm nhiều sheet
 * 1 sheet là 1 table với nhiều cells
 * Nên sử dụng các hàm GetSheet, GetCell để lấy dữ liệu.
 * Tất cả dữ liệu trong sheet đều là string, parse nếu cần.
 */