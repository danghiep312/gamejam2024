﻿#if UNITY_EDITOR
using System.IO;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using System.Diagnostics;
using UnityEngine.Networking;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Debug = UnityEngine.Debug;

namespace Base.Tool.Sheet
{
    public class SheetLoaderSO : ScriptableObject
    {
        private const string folderPath = "Assets/Resources";
        private const string fileName = nameof(SheetLoaderSO);
        private const string fileExtension = ".asset";

        public List<SpreadsheetUrl> spreadsheetUrls = new List<SpreadsheetUrl>();

        private void Reset()
        {
            this.spreadsheetUrls.Clear();
            this.spreadsheetUrls.Add(new SpreadsheetUrl(
                originalUrl: "https://docs.google.com/spreadsheets/d/1IIIjo5gRPSlIulEG8UKpblI-SoAqIkBTIZhx3srzBhw/edit#gid=0",
                deployUrl: "https://script.googleusercontent.com/macros/echo?user_content_key=jRe-oOXDPsDS82ZzSLFuPBDd60BsYvRtezlzn459IANWbRHnMxfM5h45REkORU6in3dnn-rocjSBkNGiwgLGhukQvAkSetYmm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnLGboVSRU0nSFc2Fop3Oc7jQKC2tWq342GC_0W3GI_ZxK-mDJbgixZv_K9e-8735aMAWMWL8FedDhb2fhOvJamsloigOBLgYY9z9Jw9Md8uu&lib=MRSUt4NTG0V3STyuKSFFDImlGxTIUvSrR"
            ));
        }

        [MenuItem("Development/Sheet Loader")]
        public static void OpenSheetLoader()
        {
            // create SO
            var instance = Resources.Load<SheetLoaderSO>(fileName);

            if (instance == null)
            {
                // create folder & scriptable object
                Directory.CreateDirectory(folderPath);
                AssetDatabase.CreateAsset(
                    instance = CreateInstance<SheetLoaderSO>(),
                    Path.Combine(folderPath, fileName + fileExtension)
                );
                AssetDatabase.SaveAssets();

                Debug.Log("Created " + fileName.Color("yellow") + " in " + folderPath.Color("cyan"));
            }

            Selection.activeObject = instance;
        }

        [Button(ButtonSizes.Medium)]
        public async void FetchDataFromSheet()
        {
            Debug.Log("Start fetching data ...".Color("orange"));

            foreach (var spreadsheetUrl in this.spreadsheetUrls)
            {
                // skip fetch sheet
                if (!spreadsheetUrl.enabledFetch)
                {
                    Debug.LogWarning($"Skip fetch sheet {this.spreadsheetUrls.IndexOf(spreadsheetUrl)}");
                    continue;
                }

                // fetch and calculate time
                var stopwatch = Stopwatch.StartNew();
                var spreadsheetJson = await GetDataAsync(spreadsheetUrl.deployUrl);
                stopwatch.Stop();

                Debug.Log($"Fetched sheet {this.spreadsheetUrls.IndexOf(spreadsheetUrl)} in {stopwatch.Elapsed.TotalSeconds}s".Color("cyan")
                          + "\n" + spreadsheetJson);

                // invoke action load data into game
                var spreadsheetData = JsonConvert.DeserializeObject<SpreadsheetData>(spreadsheetJson);
                spreadsheetUrl.onFetchedData?.Invoke(spreadsheetData);
            }

            Debug.Log("Fetch data completed!".Color("lime"));
        }

        private static async UniTask<string> GetDataAsync(string url)
        {
            var webRequest = UnityWebRequest.Get(url);
            webRequest.timeout = 30; // time out after 30 seconds
            await webRequest.SendWebRequest();
            return webRequest.downloadHandler.text;
        }
    }
}
#endif