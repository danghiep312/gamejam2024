using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class PopupAppear : MonoBehaviour
{
    public float startScale = 0.5f;
    public float appearDuration = 0.4f;
    public float disappearDuration = 0.3f;

    public CanvasGroup canvasGroup;

    protected virtual void OnValidate()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        transform.localScale = Vector3.one * startScale;
    }

    public virtual void Appear()
    {
        gameObject.SetActive(true);

        canvasGroup.DOKill();
        canvasGroup.DOFade(1f, appearDuration)
            .OnComplete(() => canvasGroup.interactable = true);

        transform.DOKill();
        transform.DOScale(1f, appearDuration).SetEase(Ease.OutBack);
    }

    public virtual void Disappear()
    {
        canvasGroup.interactable = false;

        canvasGroup.DOKill();
        canvasGroup.DOFade(0f, disappearDuration)
            .OnComplete(() => gameObject.SetActive(false));

        transform.DOKill();
        transform.DOScale(startScale, disappearDuration).SetEase(Ease.InBack);
    }
}