using TMPro;
using UnityEngine;
using System.Collections;

public class FpsDisplay : MonoBehaviour
{
    public TextMeshProUGUI fpsText;

    private void OnValidate()
    {
        fpsText = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
#if UNITY_EDITOR
        StartCoroutine(UpdateFPS());
#else
          gameObject.SetActive(false);
#endif
    }

    private IEnumerator UpdateFPS()
    {
        var waitRealTime = new WaitForSecondsRealtime(0.5f);

        while (true)
        {
            //! set FPS to text
            if (Time.timeScale == 0)
            {
                fpsText.text = "-.-";
                fpsText.color = Color.white;
            }
            else
            {
                int fps = Mathf.FloorToInt(1f / Time.deltaTime);
                fpsText.text = fps.ToString();
                fpsText.color = ColorByFPS(fps);
            }

            //! wait for next update
            yield return waitRealTime;
        }
    }

    private static Color ColorByFPS(int fps)
    {
        if (fps <= 30) return Color.red;
        if (fps <= 50) return Color.yellow;
        return Color.green;
    }
}