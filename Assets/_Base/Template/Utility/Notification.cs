using Base.Singleton;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Notification : Singleton<Notification>
{
    [Header("Components")] [SerializeField]
    private CanvasGroup canvasGroup;

    [SerializeField] private Transform bgImage;
    [SerializeField] private TextMeshProUGUI message;

    [SerializeField] private float appearDuration = 0.4f;
    [SerializeField] private float waitDuration = 0.8f;
    [SerializeField] private float disappearDuration = 0.4f;

    public override void Awake()
    {
    }

    private void Start()
    {
        canvasGroup.alpha = 0f;
    }

    public void Notify(string message)
    {
        this.message.text = message;

        canvasGroup.DOKill();
        canvasGroup.alpha = 0f;
        canvasGroup.DOFade(1f, appearDuration).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            canvasGroup.DOFade(0f, disappearDuration).SetDelay(waitDuration);
        });

        bgImage.DOKill();
        bgImage.localScale = new Vector3(0, 1, 0);
        bgImage.DOScaleX(1f, appearDuration).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            bgImage.DOScaleX(0f, disappearDuration).SetDelay(waitDuration);
        });
    }
}