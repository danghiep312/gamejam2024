using TMPro;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using RAND = UnityEngine.Random;

public static class Extensions
{
    #region ========== FADE ==========
    public static void Fade(this CanvasGroup canvasGroup, float alpha)
    {
        canvasGroup.alpha = 0f;
    }

    public static void Fade(this Image image, float alpha)
    {
        var color = image.color;
        color.a = alpha;
        image.color = color;
    }

    public static void Fade(this TextMeshProUGUI text, float alpha)
    {
        var color = text.color;
        color.a = alpha;
        text.color = color;
    }

    public static void Fade(this SpriteRenderer renderer, float alpha)
    {
        var color = renderer.color;
        color.a = alpha;
        renderer.color = color;
    }

    public static void Fade(this GameObject obj, float alpha)
    {
        var spriteRenderer = obj.GetComponent<SpriteRenderer>();
        if (spriteRenderer)
            spriteRenderer.Fade(alpha);

        foreach (Transform child in obj.transform)
            Fade(child.gameObject, alpha);
    }

    public static void DoFade(this GameObject obj, float alpha, float duration = 0f, float delay = 0f)
    {
        var spriteRenderer = obj.GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.DOKill();
            spriteRenderer.DOFade(alpha, duration).SetDelay(delay);
        }

        foreach (Transform child in obj.transform)
            DoFade(child.gameObject, alpha, duration, delay);
    }
    #endregion


    #region ========== RANDOM ==========
    public static int Rand(int min, int max)
    {
        return RAND.Range(min, max + 1);
    }

    public static T Rand<T>(this IList<T> list, T currentValue = default)
    {
        if (list.Count == 0)
        {
            Debug.LogError("<color=red> IList is empty </color>");
        }

        // random giá trị mới cho tới khi nào khác current value
        // khi không truyền vào currentValue, mặc định là null
        while (true)
        {
            var newValue = list[RAND.Range(0, list.Count)];
            if (!newValue.Equals(currentValue))
            {
                return newValue;
            }
        }
    }

    public static T Rand<T>(this T[] array)
    {
        return array[RAND.Range(0, array.Length)];
    }
    #endregion


    #region ========== MATH ==========
    public static int Millisecond(this float second)
    {
        return Mathf.FloorToInt(second * 1000);
    }

    public static int Round(this int a, int b)
    {
        return Mathf.Round(1f * a / b).Int();
    }

    public static int Int(this float x)
    {
        return Mathf.RoundToInt(x);
    }

    public static int Int(this TextMeshProUGUI text)
    {
        return float.Parse(text.text).Int();
    }

    public static int Percent(this float x)
    {
        return (100 * x).Int();
    }

    public static bool InRange(this int value, int min, int max)
    {
        return value >= min && value <= max;
    }

    public static bool InRange(this float value, float min, float max)
    {
        return value >= min && value <= max;
    }
    #endregion


    #region ========== ARRAY ==========
    public static void Fill<T>(this T[] array, T value)
    {
        for (var i = 0; i < array.Length; i++)
        {
            array[i] = value;
        }
    }

    public static void Fill<T>(this T[,] array, T value)
    {
        for (var i = 0; i < array.GetLength(0); i++)
        {
            for (var j = 0; j < array.GetLength(1); j++)
            {
                array[i, j] = value;
            }
        }
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        var n = list.Count;
        while (n > 1)
        {
            n--;
            var k = Random.Range(0, n + 1);
            (list[k], list[n]) = (list[n], list[k]);
        }
    }
    #endregion

    public static string Color(this string str, string color)
    {
        return $"<color={color}>{str}</color>";
    }


    public static void SetPosXY(this Transform currentPos, Vector3 newPos)
    {
        currentPos.position = new Vector3(newPos.x, newPos.y, currentPos.position.z);
    }
}

public static class StringExtensions
{
    public static string Size(this string str, int size)
    {
        return $"<size={size}>{str}</size>";
    }

    public static string Bold(this string str)
    {
        return $"<b>{str}</b>";
    }

    public static string Italic(this string str)
    {
        return $"<i>{str}</i>";
    }
}