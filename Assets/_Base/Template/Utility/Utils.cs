﻿using System;
using System.Text;
using UnityEngine;
using Cysharp.Threading.Tasks;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using Base.Pool;
using Base.Sound;
using UnityEngine.UI;

public static class Utils
{
    /// <summary>
    /// kiểm tra xem chuột có đang nằm trên UI hay không?
    /// UI có tick chọn RaycastTarget
    /// </summary>
    private static PointerEventData _eventDataCurrentPosition;

    private static List<RaycastResult> _results;

    public static bool IsMouseOverUI()
    {
        _eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        _eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        _results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(_eventDataCurrentPosition, _results);
        return _results.Count > 0;
    }


    /// <summary>
    /// lấy vị trí chuột theo đơn vị World
    /// </summary>
    private static Camera _mainCam;

    public static Vector3 GetMouseWorldPosition()
    {
        if (_mainCam == null)
            _mainCam = Camera.main;

        var mousePos = Input.mousePosition;
        var mouseWorldPos = _mainCam!.ScreenToWorldPoint(mousePos); // z = -10
        return new Vector3(mouseWorldPos.x, mouseWorldPos.y);
    }


    public static Vector3 WorldToScreenPoint(Vector3 worldPos)
    {
        if (_mainCam == null)
            _mainCam = Camera.main;

        return _mainCam!.WorldToScreenPoint(worldPos);
    }


    public static int Rand(int min, int max)
    {
        return UnityEngine.Random.Range(min, max + 1);
    }

    public static float Rand(float min, float max)
    {
        return UnityEngine.Random.Range(min, max);
    }


    public static Vector3 GetPivot(Sprite sprite)
    {
        var pivotInPixels = sprite.pivot;
        var spriteRect = sprite.rect;
        return new Vector2(pivotInPixels.x / spriteRect.width, pivotInPixels.y / spriteRect.height);
    }



    #region ========== DELAY & WAIT ==========
    public static async void DelayAction(int millisecond, Action action)
    {
        await UniTask.Delay(millisecond);
        action?.Invoke();
    }

    public static async void DelayFrameAction(int frame, Action action)
    {
        await UniTask.DelayFrame(frame);
        action?.Invoke();
    }

    public static UniTask WaitAction(int millisecond, Action action)
    {
        action?.Invoke();
        return UniTask.Delay(millisecond);
    }

    public static UniTask WaitFrameAction(int frame, Action action)
    {
        action?.Invoke();
        return UniTask.DelayFrame(frame);
    }
    #endregion



    public static string ConvertToConst(string input)
    {
        // Tách chuỗi thành các từ riêng lẻ
        var separators = new char[] { ' ', '_' };
        var words = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);

        for (var i = 0; i < words.Length; i++)
        {
            #region Xử lý UniKey => Uni_Key
            var newWord = new StringBuilder();
            var lastCharWasLower = false;

            foreach (var c in words[i])
            {
                if (char.IsUpper(c))
                {
                    if (lastCharWasLower)
                    {
                        newWord.Append('_');
                    }

                    lastCharWasLower = false;
                }
                else
                {
                    lastCharWasLower = true;
                }

                newWord.Append(c);
            }
            #endregion

            words[i] = newWord.ToString();
        }

        return string.Join("_", words).ToUpper();
    }
}