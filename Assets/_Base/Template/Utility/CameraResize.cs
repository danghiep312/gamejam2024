﻿using UnityEngine;
using Sirenix.OdinInspector;

[ExecuteInEditMode]
public class CameraResize : MonoBehaviour
{
    public SpriteRenderer rink;

    private void Awake()
    {
        ResizeCamera();
    }

    [Button("Resize")]
    private void ResizeCamera()
    {
        // * FOR VERTICAL FIT
        //Camera.main.orthographicSize = _screenBound.bounds.size.x * Screen.height / Screen.width * 0.5f;

        // * FOR HORIZONTAL FIT
        //Camera.main.orthographicSize = _screenBound.bounds.size.y / 2;

        // * FOR ENTIRE FIT
        var screenRatio = (float)Screen.width / Screen.height;
        var targetRatio = rink.bounds.size.x / rink.bounds.size.y;

        if (screenRatio >= targetRatio)
        {
            Camera.main!.orthographicSize = rink.bounds.size.y / 2;
        }
        else
        {
            var differenceInSize = targetRatio / screenRatio;
            Camera.main!.orthographicSize = rink.bounds.size.y / 2 * differenceInSize;
        }
    }
}