using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasShowHide : MonoBehaviour
{
     private void Awake()
     {
          var canvasGroup = GetComponent<CanvasGroup>();
          canvasGroup.alpha = 1;
     }

     [ButtonGroup, GUIColor(0f, 1f, 0f)]
     public void Show()
     {
          GetComponent<CanvasGroup>().alpha = 1;
     }

     [ButtonGroup, GUIColor(1f, 0.2f, 0f)]
     public void Hide()
     {
          GetComponent<CanvasGroup>().alpha = 0;
     }
}