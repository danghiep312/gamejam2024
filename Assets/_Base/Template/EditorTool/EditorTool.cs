﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Base.Utility
{
    public class EditorTool : MonoBehaviour
    {
        [MenuItem("☆☆☆/Clear Save Data")]
        private static void ClearData()
        {
            PlayerPrefs.DeleteAll(); // Xóa PlayerPref
            Debug.Log("Data cleared!".Color("lime"));
        }

        [MenuItem("☆☆☆/Play Immediately")]
        public static void SwitchToSceneLoadingAndPlay()
        {
            EditorSceneManager.SaveOpenScenes(); // Lưu scene hiện tại
            EditorSceneManager.SaveOpenScenes(); // Lưu scene hiện tại
            var scenePath = SceneUtility.GetScenePathByBuildIndex(0);
            EditorSceneManager.OpenScene(scenePath);
            EditorApplication.isPlaying = true; // Bật chế độ chơi game
        }

        [MenuItem("☆☆☆/Switch Next Scene")]
        public static void SwitchNextScene()
        {
            var currentScene = SceneManager.GetActiveScene();
            var nextSceneIndex = currentScene.buildIndex + 1;
            if (nextSceneIndex >= SceneManager.sceneCountInBuildSettings)
                nextSceneIndex = 0;
            var nextScenePath = SceneUtility.GetScenePathByBuildIndex(nextSceneIndex);
            EditorSceneManager.OpenScene(nextScenePath);
            Debug.Log($"Switch to scene {nextSceneIndex}!".Color("lime"));
        }
    }
}
#endif