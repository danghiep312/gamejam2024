#if UNITY_EDITOR
using System.IO;
using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace Base.Tool.Build
{
    public class PostprocessBuild : IPostprocessBuildWithReport
    {
        public int callbackOrder => 0;

        public void OnPostprocessBuild(BuildReport report)
        {
            Debug.Log($"BUILD RESULT: {report.summary.result}".Color("yellow")
                      + "\n" + report.summary.outputPath);

            var fileExtension = Path.GetExtension(report.summary.outputPath);

            // Open Google Drive folder
            switch (fileExtension)
            {
                case ".aab":
                    Application.OpenURL(BuildSettingsSO.GetInstance().aabBuildFolderURL);
                    break;
                case ".apk":
                    Application.OpenURL(BuildSettingsSO.GetInstance().apkBuildFolderURL);
                    break;
            }
        }
    }
}
#endif