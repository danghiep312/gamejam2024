#if UNITY_EDITOR
using System.IO;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace Base.Tool.Build
{
    public class BuildSettingsSO : ScriptableObject
    {
        private const string folderPath = "Assets/Resources";
        private const string fileName = nameof(BuildSettingsSO);
        private const string fileExtension = ".asset";

        [Title("Android Version")]
        public string androidVersionName = "1.0.1";
        public int androidVersionCode = 1;

        [Title("iOS Version")]
        public string iOSVersionName = "1.0.1";
        public int iOSVersionCode = 1;

        [Title("Password")]
        public string keystorePassword;
        public string keyaliasPass;

        [Title("GG Drive")]
        [InlineButton(nameof(OpenApkBuildFolder), "Open")]
        public string apkBuildFolderURL;
        [InlineButton(nameof(OpenAabBuildFolder), "Open")]
        public string aabBuildFolderURL;

        private void OnValidate()
        {
            PlayerSettings.bundleVersion = this.androidVersionName;
            PlayerSettings.Android.bundleVersionCode = this.androidVersionCode;
            PlayerSettings.Android.keyaliasPass = this.keyaliasPass;
            PlayerSettings.Android.keystorePass = this.keystorePassword;
        }

        [MenuItem("Development/Build Settings")]
        public static BuildSettingsSO GetInstance()
        {
            var instance = Resources.Load<BuildSettingsSO>(fileName);

            if (instance == null)
            {
                // create folder & scriptable object
                Directory.CreateDirectory(folderPath);
                AssetDatabase.CreateAsset(
                    instance = CreateInstance<BuildSettingsSO>(),
                    Path.Combine(folderPath, fileName + fileExtension)
                );
                AssetDatabase.SaveAssets();

                Debug.Log("Created " + fileName.Color("yellow") + " in " + folderPath.Color("cyan"));
            }

            Selection.activeObject = instance;
            return instance;
        }

        [Button(ButtonSizes.Medium), ButtonGroup("0")]
        public void BuildAab()
        {
            PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;
            EditorUserBuildSettings.buildAppBundle = true;
            EditorApplication.ExecuteMenuItem("File/Build Settings...");
        }

        [Button(ButtonSizes.Medium), ButtonGroup("0")]
        public void BuildApk()
        {
            PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.Mono2x);
            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7;
            EditorUserBuildSettings.buildAppBundle = false;
            EditorApplication.ExecuteMenuItem("File/Build Settings...");
        }

        public void OpenApkBuildFolder() => Application.OpenURL(this.apkBuildFolderURL);

        public void OpenAabBuildFolder() => Application.OpenURL(this.aabBuildFolderURL);
    }
}
#endif