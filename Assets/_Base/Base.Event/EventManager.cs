using System;
using System.Collections.Generic;

public static class EventManager
{
    private static readonly Dictionary<EventName, Action<Message>> eventDictionary = new Dictionary<EventName, Action<Message>>();

    public static void StartListening(EventName eventName, Action<Message> listener)
    {
        if (eventDictionary.TryGetValue(eventName, out var listeners))
        {
            listeners += listener;
            eventDictionary[eventName] = listeners;
        }
        else
        {
            listeners += listener;
            eventDictionary.Add(eventName, listeners);
        }
    }

    public static void StopListening(EventName eventName, Action<Message> listener)
    {
        if (eventDictionary.TryGetValue(eventName, out var listeners))
        {
            listeners -= listener;
            eventDictionary[eventName] = listeners;
        }
    }

    public static void TriggerEvent(EventName eventName, Message message = null)
    {
        if (eventDictionary.TryGetValue(eventName, out var listener))
        {
            listener.Invoke(message);
        }
    }
}


// Help short code
public class Message : Dictionary<string, object>
{
}