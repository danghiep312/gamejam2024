public enum EventName
{
    OnStartGame,
    OnCoinChanged,
    OnOpenStar,
    
    OnPlayDailyChallenge,
    OnSetupDailyChallenge,
    
    OnGetMoreBooster,
}