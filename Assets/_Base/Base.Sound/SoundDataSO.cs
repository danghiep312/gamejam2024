﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Base.Sound
{
    [CreateAssetMenu(fileName = nameof(SoundDataSO), menuName = "GameData/SoundData")]
    public partial class SoundDataSO : ScriptableObject
    {
        [Title("Sound Prefs")]
        [ShowInInspector, PropertyOrder(-1)] private bool MusicOn => SoundPrefs.MusicOn;
        [ShowInInspector, PropertyOrder(-1)] private bool SoundOn => SoundPrefs.SoundOn;
        [ShowInInspector, PropertyOrder(-1)] private bool VibrationOn => SoundPrefs.VibrationOn;

        [Title("List Sounds")]
        public List<SoundGroup> soundGroups = new List<SoundGroup>();
    }


    [Serializable]
    public class SoundGroup
    {
        [HorizontalGroup("0", Width = 0.15f)]
        [HideLabel, GUIColor("#ffff80")]
        public string groupName;

        [HorizontalGroup("0")]
        [LabelText("$groupName")]
        public List<Sound> listSound = new List<Sound>();
    }


    [Serializable]
    public class Sound
    {
        public AudioClip clip;

        [Range(0f, 1f)]
        public float volume = 1f;

        [HideInInspector]
        public float pitch = 1f;

        public bool isMusic;
    }
}