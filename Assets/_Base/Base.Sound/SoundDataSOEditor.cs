﻿#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Base.Sound
{
    public partial class SoundDataSO
    {
        [Title("Editor Tools")]
        [ShowInInspector] private const string SOUND_NAMESPACE = "Base.Sound";
        [ShowInInspector] private const string SOUND_NAME = "SoundName";

        [HorizontalGroup("0")]
        [Button, GUIColor(0.8f, 0.4f, 0f)]
        public void GenerateSoundNameScript()
        {
            // find the directory where the script was generated
            var g = AssetDatabase.FindAssets($"t:Script {nameof(SoundManager)}");
            var scriptPath = AssetDatabase.GUIDToAssetPath(g[0]);
            var folderPath = System.IO.Path.GetDirectoryName(scriptPath);
            Debug.Log("Script will be generated in: ".Color("cyan") + folderPath.Color("yellow"));

            // start building the class
            var classBuilder = new System.Text.StringBuilder();
            classBuilder.AppendLine($"namespace {SOUND_NAMESPACE}");
            classBuilder.AppendLine("{");
            classBuilder.AppendLine($"\tpublic static class {SOUND_NAME}");
            classBuilder.AppendLine("\t{");

            foreach (var soundGroup in soundGroups)
            {
                classBuilder.AppendLine($"\t\t// ===== {soundGroup.groupName} =====");

                var soundNames = soundGroup.listSound.Select(s => s.clip.name).ToList();
                foreach (var soundName in soundNames)
                {
                    var fieldName = Utils.ConvertToConst(soundName);
                    classBuilder.AppendLine($"\t\tpublic const string {fieldName} = \"{soundName}\";");
                }

                if (soundGroup != soundGroups.Last())
                    classBuilder.AppendLine();
            }

            classBuilder.AppendLine("\t}");
            classBuilder.AppendLine("}");

            // write to the file
            var scriptContent = classBuilder.ToString();
            var path = $"{folderPath}/{SOUND_NAME}.cs";
            System.IO.File.WriteAllText(path, scriptContent);
            Debug.Log($"Generated class {SOUND_NAME}\n".Color("lime") + scriptContent);
        }


        [HorizontalGroup("0")]
        [Button, GUIColor(0f, 0.8f, 0.4f)]
        public static void OpenSoundNameScript()
        {
            // find the directory where the script was generated
            var g = AssetDatabase.FindAssets($"t:Script {SOUND_NAME}");
            var scriptPath = AssetDatabase.GUIDToAssetPath(g[0]);

            // Tải tệp script dựa trên đường dẫn
            var scriptObject = AssetDatabase.LoadAssetAtPath(scriptPath, typeof(Object));

            if (scriptObject != null)
                Selection.activeObject = scriptObject;
            else
                Debug.LogError("Cannot find script file at path: " + scriptPath);
        }
    }
}
#endif