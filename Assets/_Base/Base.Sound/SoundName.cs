namespace Base.Sound
{
	public static class SoundName
	{
		// ===== Musics =====
		public const string MENU = "menu";
		public const string GAMEPLAY = "gameplay";
		public const string ENDGAME = "endgame";

		// ===== SFX =====
		public const string COLLECT_BUFF = "collect_buff";
		public const string HURT5 = "hurt5";
		public const string BUTTON_ON = "button on";
		public const string BUTTON_OFF = "button off";
	}
}
