﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using MoreMountains.NiceVibrations;
using Base.Singleton;

namespace Base.Sound
{
    public class SoundManager : Base.Singleton.Singleton<SoundManager>
    {
        [SerializeField]
        private SoundDataSO soundData;

        private static readonly List<Sound> allSounds = new List<Sound>();
        private static readonly List<AudioSource> audioSourcePool = new List<AudioSource>();


        protected override void OnAwake()
        {
            if (SoundPrefs.IsFirstTimeInGame)
            {
                Debug.Log("[SoundManager] first time in game, set ON for all settings");
                SoundPrefs.SetDefaultSettings();
            }

            // merge all sounds into one list
            allSounds.AddRange(soundData.soundGroups.SelectMany(group => group.listSound));
        }


        private static AudioSource GetAudioSourceForSound(Sound sound)
        {
            // find free audioSource in pool
            var audioSource = audioSourcePool.Find(source => !source.isPlaying);

            // add new AudioSource component into pool
            if (audioSource == null)
            {
                audioSource = Instance.gameObject.AddComponent<AudioSource>();
                audioSourcePool.Add(audioSource);
            }

            // set value of sound for audioSource
            audioSource.loop = false;
            audioSource.clip = sound.clip;
            audioSource.pitch = sound.pitch;
            audioSource.volume = sound.volume;

            return audioSource;
        }


        private static Sound FindSoundByName(string soundName)
        {
            var sound = allSounds.Find(sound => sound.clip.name == soundName);

            if (sound == null)
            {
                Debug.LogWarning("Can't find sound with name " + soundName.Color("red"));
                return null;
            }

            return sound;
        }


        private static Sound FindMusicByName(string musicName)
        {
            var music = allSounds.Find(sound => sound.isMusic && sound.clip.name == musicName);

            if (music == null)
            {
                Debug.LogWarning("Can't find music with name " + musicName.Color("red"));
                return null;
            }

            return music;
        }


        public static void PlaySound(string soundName)
        {
            if (!SoundPrefs.SoundOn) return;

            var sound = FindSoundByName(soundName);

            if (sound != null)
            {
                var audioSource = GetAudioSourceForSound(sound);
                audioSource.Play();
            }
        }


        public static void PlaySoundOneShot(string soundName)
        {
            if (!SoundPrefs.SoundOn) return;

            var sound = FindSoundByName(soundName);

            if (sound != null)
            {
                // can use only one audioSource to play one shot
                var audioSource = audioSourcePool[0];
                audioSource.PlayOneShot(sound.clip);
            }
        }


        public static void PlaySoundInTime(string soundName, float duration)
        {
            if (!SoundPrefs.SoundOn) return;

            var sound = FindSoundByName(soundName);

            if (sound != null)
            {
                var audioSource = GetAudioSourceForSound(sound);
                audioSource.Play();

                // wait to stop sound
                Utils.DelayAction(duration.Millisecond(), () => audioSource.Stop());
            }
        }


        public static void PlayMusic(string musicName, bool loop = false)
        {
            if (!SoundPrefs.MusicOn) return;

            var music = FindMusicByName(musicName);

            if (music != null)
            {
                var audioSource = GetAudioSourceForSound(music);
                audioSource.loop = loop;
                audioSource.Play();
            }
        }


        public static void PlayMusicInTime(string musicName, float duration)
        {
            if (!SoundPrefs.MusicOn) return;

            var music = FindMusicByName(musicName);

            if (music != null)
            {
                var audioSource = GetAudioSourceForSound(music);
                audioSource.loop = true;
                audioSource.Play();

                // wait to stop music
                Utils.DelayAction(duration.Millisecond(), () => audioSource.Stop());
            }
        }


        public static void StopMusic(string musicName)
        {
            var audioSourcePlayingMusic = audioSourcePool.Find(
                source => source.isPlaying && source.clip.name == musicName);

            audioSourcePlayingMusic?.Stop();
        }


        public static void StopAllMusic()
        {
            Debug.Log("[SoundManager] stop all musics".Color("orange"));

            allSounds.FindAll(sound => sound.isMusic)
                .ForEach(music => StopMusic(music.clip.name));
        }


        public static void ChangePitch(float value)
        {
            audioSourcePool.ForEach(audioSource => audioSource.pitch = value);
        }


        public static void Vibrate(HapticTypes type)
        {
            if (SoundPrefs.VibrationOn)
                MMVibrationManager.Haptic(type);
        }
    }
}